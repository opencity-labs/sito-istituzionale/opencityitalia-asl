<?php /* #?ini charset="utf-8"?

[CookiesSettings]
Consent=advanced

[InstanceSettings]
NomeAmministrazioneAfferente=
UrlAmministrazioneAfferente=
LiveIPList[]
InstallerDirectory=./vendor/opencity-labs/opencity-asl-installer

[StanzaDelCittadinoBridge]
AutoDiscover=enabled

[GeneralSettings]
tag_line=
theme=asl
AutoDiscoverProfileLinks=enabled

[NetworkSettings]
PrototypeUrl=

[CreditsSettings]
Name=OpenCityItalia - ASL
Url=https://opencityitalia.it/
Title=OpenCity Italia - Servizi pubblici digitali per la tua città
CodeVersion=v
CodeUrl=https://gitlab.com/opencity-labs/sito-istituzionale/opencityitalia-asl
IsOpenCityFork=true
UseDearOldCreditsTemplate=enabled

[ViewSettings]
AvailableView[]=card
AvailableView[]=card_children
AvailableView[]=banner
AvailableView[]=card_teaser
AvailableView[]=card_image
AvailableView[]=banner_color
AvailableView[]=accordion

[Stili]
UsaNeiBlocchi=enabled
Nodo_NomeStile[]
Nodo_NomeStile[]=0;bg-100
Nodo_NomeStile[]=0;section section-muted section-inset-shadow pb-5

[TopMenu]
NodiAreeCustomMenu[]
MaxRecursion=3

[WebsiteToolbar]
ShowMediaRoot=enabled
ShowUsersRoot=disabled
ShowEditorRoles=enabled

[Attributi]
AttributiAbstract[]
AttributiAbstract[]=abstract
AttributiAbstract[]=intro
AttributiAbstract[]=short_description
AttributiAbstract[]=fonte
AttributiAbstract[]=topic
AttributiAbstract[]=tags
AttributiAbstract[]=body
AttributiAbstract[]=edizione
AttributiAbstract[]=text
AttributiAbstract[]=description
AttributiAbstract[]=event_abstract

[HideRelationsTitle]
AttributeIdentifiers[]

[SideMenu]
AmministrazioneTrasparenteTipoMenu=browsable

[AttributeHandlers]
UniqueStringCheck[]=document/has_code
UniqueStringCheck[]=lotto/cig
UniqueStringCheck[]=public_service/identifier
InputValidators[]
InputValidators[]=DocumentFileValidator
InputValidators[]=PublicServiceStatusValidator
InputValidators[]=PublicServiceProcessingTimeValidator
InputValidators[]=UniqueStringValidator
InputValidators[]=EventCostsValidator
InputValidators[]=EventContactPointValidator
InputValidators[]=EventPlacesValidator
InputValidators[]=OrganizationValidator
InputValidators[]=EventOrganizationValidator

[MotoreRicerca]
IncludiClassi[]
IncludiClassi[]=topic
IncludiClassi[]=article
IncludiClassi[]=dataset
IncludiClassi[]=document
IncludiClassi[]=event
IncludiClassi[]=organization
IncludiClassi[]=person
IncludiClassi[]=public_person
IncludiClassi[]=pagina_sito
IncludiClassi[]=pagina_trasparenza
IncludiClassi[]=online_contact_point
IncludiClassi[]=public_service
IncludiClassi[]=trasparenza
IncludiClassi[]=bando_concorso
IncludiClassi[]=howto
IncludiClassi[]=faq_section
IncludiClassi[]=faq_group
*/?>
